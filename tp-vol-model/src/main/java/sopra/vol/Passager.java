package sopra.vol;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
public class Passager {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	@NotEmpty(message = "L'adresse email est obligatoire")
	@Email
	private String mail;
	
	@Column
	@NotEmpty(message = "Le numéro de téléphone est obligatoire")
	private String telephone;
	
	@Column
	private Civilite civilite;
	
	@Column
	@NotEmpty(message = "Le nom est obligatoire")
	@Size(min = 2, max = 100, message = "Le nom doit comporter entre 2 et 100 caractères")
	private String nom;
	
	@Column
	@NotEmpty(message = "Le prénom est obligatoire")
	@Size(min = 2, max = 100, message = "Le prénom doit comporter entre 2 et 100 caractères")
	private String prenom;
	
	@Column
	@Temporal(TemporalType.DATE)
	@Past
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@NotNull(message = "La date de naissance est obligatoire")
	private Date dtNaissance;
	
	@Column
	@NotEmpty(message = "La nationalité doit être renseignée")
	private String nationalite;
	
	@Column
	private TypePieceIdentite typePI;
	
	@Column
	@Temporal(TemporalType.DATE)
	@Future
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateValiditePI;
	
	@Column
	@Embedded
	@NotEmpty(message = "L'adresse est obligatoire")
	private String adressePrincipale;
	
	@Version
	private int version;
	
	@Column
	@OneToMany(mappedBy = "passager")
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Passager() {
		super();
	}

	public Passager(Long id, String mail, String telephone, Civilite civilite, String nom, String prenom,
			Date dtNaissance, String nationalite, TypePieceIdentite typePI, Date dateValiditePI) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.dtNaissance = dtNaissance;
		this.nationalite = nationalite;
		this.typePI = typePI;
		this.dateValiditePI = dateValiditePI;
	}
	
	public Passager(Long id, @NotEmpty(message = "L'adresse email est obligatoire") @Email String mail,
			@NotEmpty(message = "Le numéro de téléphone est obligatoire") String telephone, Civilite civilite,
			@NotEmpty(message = "Le nom est obligatoire") @Size(min = 2, max = 100, message = "Le nom doit comporter entre 2 et 100 caractères") String nom,
			@NotEmpty(message = "Le prénom est obligatoire") @Size(min = 2, max = 100, message = "Le prénom doit comporter entre 2 et 100 caractères") String prenom,
			@Past @NotNull(message = "La date de naissance est obligatoire") Date dtNaissance,
			@NotEmpty(message = "La nationalité doit être renseignée") String nationalite, TypePieceIdentite typePI,
			@Future Date dateValiditePI, @NotEmpty(message = "L'adresse est obligatoire") String adressePrincipale) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.dtNaissance = dtNaissance;
		this.nationalite = nationalite;
		this.typePI = typePI;
		this.dateValiditePI = dateValiditePI;
		this.adressePrincipale = adressePrincipale;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDtNaissance() {
		return dtNaissance;
	}

	public void setDtNaissance(Date dtNaissance) {
		this.dtNaissance = dtNaissance;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public TypePieceIdentite getTypePI() {
		return typePI;
	}

	public void setTypePI(TypePieceIdentite typePI) {
		this.typePI = typePI;
	}

	public Date getDateValiditePI() {
		return dateValiditePI;
	}

	public void setDateValiditePI(Date dateValiditePI) {
		this.dateValiditePI = dateValiditePI;
	}

	public String getAdressePrincipale() {
		return adressePrincipale;
	}

	public void setAdressePrincipale(String adressePrincipale) {
		this.adressePrincipale = adressePrincipale;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}

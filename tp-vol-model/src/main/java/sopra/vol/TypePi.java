package sopra.vol;

public enum TypePi {
	CNI("typePI.CNI"), PASSEPORT("typePI.PASSEPORT"), PERMIS("typePI.PERMIS"), VISA("typePI.VISA");

	private final String label;

	private TypePi(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

package sopra.vol;

public enum TypeEntreprise {
	SA("typeEntreprise.SA"),SAS("typeEntreprise.SAS"),SASU("typeEntreprise.SASU"),SARL("typeEntreprise.SARL"),EURL("typeEntreprise.EURL");
	
	private final String label;

	private TypeEntreprise(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}

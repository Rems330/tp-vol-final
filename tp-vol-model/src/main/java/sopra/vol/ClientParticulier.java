package sopra.vol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Version;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class ClientParticulier extends Client {
	@Column(name = "civility")
	@Enumerated(EnumType.STRING)
	@NotNull(message = "La civilité est obligatoire")
	private Civilite civilite;
	@Column(name = "last_name", length = 100)
	@NotEmpty(message = "Le nom est obligatoire")
	@Size(min = 2, max = 100, message = "Le nom doît comporter au moins 2 caractères et aux maximum 100 !")
	@Pattern(regexp = "^[A-Z].*$", message = "Le nom doît commencer par une majuscule !")
	private String nom;
	@Column(name = "first_name", length = 100)
	@NotEmpty(message = "Le prénom est obligatoire")
	@Size(min = 2, max = 100, message = "Le prénom doît comporter au moins 2 caractères et aux maximum 100 !")
	private String prenom;

	public ClientParticulier() {
		super();
	}

	public ClientParticulier(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, Civilite civilite,
			String nom, String prenom) {
		super(id, mail, telephone, moyenPaiement);
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	

	public ClientParticulier(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, int version, Civilite civilite, String prenom, String nom) {
		super(id, mail, telephone, moyenPaiement);
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}

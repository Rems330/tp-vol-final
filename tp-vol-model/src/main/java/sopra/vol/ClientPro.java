package sopra.vol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;


@Entity
public class ClientPro extends Client {
	@Column(name = "num_siret")
//	@NotEmpty(message = "Le numéro SIRET est obligatoire")
	private String numeroSiret;
	@Column(name = "name_enterprise")
//	@NotEmpty(message = "Le nom est obligatoire")
	private String nomEntreprise;
	@Column(name = "num_tva")
	private String numTVA;
	@Column(name = "type_enterprise")
	@Enumerated(EnumType.STRING)
	private TypeEntreprise typeEntreprise;

	public ClientPro() {
		super();
	}

	public ClientPro(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, String numeroSiret,
			String nomEntreprise, String numTVA, TypeEntreprise typeEntreprise) {
		super(id, mail, telephone, moyenPaiement);
		this.numeroSiret = numeroSiret;
		this.nomEntreprise = nomEntreprise;
		this.numTVA = numTVA;
		this.typeEntreprise = typeEntreprise;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumTVA() {
		return numTVA;
	}

	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}

	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}


}

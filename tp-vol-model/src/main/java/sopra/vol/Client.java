package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Client {
	@Id
	@GeneratedValue
	private Long id;
	@Version
	private int version;
	@Column(length = 50)
	@Size(max = 50, message = "L''email doît comporter au moins 1 caractères et aux maximum 50 !")
	@NotEmpty(message = "L''email est obligatoire")
	@Email(message = "Email invalide")
	private String mail;
	@Column(length = 20)
	@Size(max = 50, message = "Le numéro de téléphone doît comporter au moins 1 caractères et aux maximum 20 !")
	@NotEmpty(message = "Le numéro de téléphone est obligatoire")
	private String telephone;
	@Column
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Le type de moyen de payement est obligatoire")
	private MoyenPaiement moyenPaiement;
	@Column
	@Embedded
	private Adresse principale;
	@Column
	@Embedded
	@AttributeOverrides ({ @AttributeOverride( name = "voie", column = @Column(name = "voie_facturation")), 
		@AttributeOverride( name = "complement", column = @Column(name = "complement_facturation")),
		@AttributeOverride( name = "codePostal", column = @Column(name = "CP_facturation")),
		@AttributeOverride( name = "ville", column = @Column(name = "ville_facturation")),
		@AttributeOverride( name = "pays", column = @Column(name = "pays_facturation"))})
	private Adresse facturation;
	@Column
	@OneToMany(mappedBy = "client")
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Client() {
		super();
	}

	public Client(Long id, String mail, String telephone, MoyenPaiement moyenPaiement) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.moyenPaiement = moyenPaiement;
	}
	
	public Client(Long id, int version, String mail, String telephone,
			MoyenPaiement moyenPaiement, Adresse principale, Adresse facturation, List<Reservation> reservations) {
		super();
		this.id = id;
		this.version = version;
		this.mail = mail;
		this.telephone = telephone;
		this.moyenPaiement = moyenPaiement;
		this.principale = principale;
		this.facturation = facturation;
		this.reservations = reservations;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public MoyenPaiement getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	public Adresse getFacturation() {
		return facturation;
	}

	public void setFacturation(Adresse facturation) {
		this.facturation = facturation;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	
	

}

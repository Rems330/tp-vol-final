package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Client;
import sopra.vol.ClientPro;
import sopra.vol.MoyenPaiement;
import sopra.vol.TypeEntreprise;
import sopra.vol.dao.IClientRepository;

@Controller
@RequestMapping("/clientpro")
public class ClientProController {
	@Autowired
	private IClientRepository clientProRepo;

	public ClientProController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesClientPros", clientProRepo.findAll());

		return "clientpro/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monClientPro", new ClientPro());
		model.addAttribute("moyenPaiements", MoyenPaiement.values());
		model.addAttribute("typeEntreprises", TypeEntreprise.values());

		return "clientpro/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Client> optClientPro = clientProRepo.findById(id);

		if (optClientPro.isPresent()) {
			model.addAttribute("monClientPro", optClientPro.get());
		}

		model.addAttribute("moyenPaiements", MoyenPaiement.values());
		model.addAttribute("typeEntreprises", TypeEntreprise.values());

		return "clientpro/form";
	}

//	@PostMapping("/save")
//	public String save(@RequestParam Long id, @RequestParam int version, @RequestParam Civilite civilite, @RequestParam String nom, @RequestParam String prenom, @RequestParam int age, @RequestParam String from) {
//
//		Eleve eleve = null;
//
//		if (from.contentEquals("add")) {
//			eleve = new Eleve(civilite, nom, prenom, age);
//		} else {
//			eleve = new Eleve(id, version, civilite, nom, prenom, age);
//		}
//
//		eleveRepo.save(eleve);
//
//		return "redirect:list";
//	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monClientPro") @Valid ClientPro clientpro, BindingResult result, Model model) {
		new ClientProValidator().validate(clientpro, result);

		if (result.hasErrors()) {
			model.addAttribute("moyenPaiements", MoyenPaiement.values());
			model.addAttribute("typeEntreprises", TypeEntreprise.values());

			return "clientpro/form";
		}

		clientProRepo.save(clientpro);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		clientProRepo.deleteById(id);

		return "redirect:/clientpro/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}

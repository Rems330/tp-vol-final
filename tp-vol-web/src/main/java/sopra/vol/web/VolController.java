package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Vol;
import sopra.vol.dao.IVolRepository;

@Controller
@RequestMapping("/vol")
public class VolController {
	@Autowired
	private IVolRepository VolRepo;

	public VolController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesVols", VolRepo.findAll());

		return "vol/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monVol", new Vol());

		return "vol/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Vol> optVol = VolRepo.findById(id);

		if (optVol.isPresent()) {
			model.addAttribute("monVol", optVol.get());
		}

		return "vol/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monVol") @Valid Vol Vol, BindingResult result, Model model) {
		new VolValidator().validate(Vol, result);

		VolRepo.save(Vol);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		VolRepo.deleteById(id);

		return "redirect:/vol/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}

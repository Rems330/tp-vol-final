package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Reservation;
import sopra.vol.dao.IReservationRepository;

@Controller
@RequestMapping("/reservation")
public class ReservationController {
	
	@Autowired
	private IReservationRepository reservationRepo;

	public ReservationController() {
		super();
	}
	
	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesReservations", reservationRepo.findAll());

		return "reservation/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("maReservation", new Reservation());

		return "reservation/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Reservation> optReservation = reservationRepo.findById(id);

		if (optReservation.isPresent()) {
			model.addAttribute("maReservation", optReservation.get());
		}

		return "reservation/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("maReservation") @Valid Reservation reservation, BindingResult result, Model model) {
		new ReservationValidator().validate(reservation, result);

		if (result.hasErrors()) {

			return "reservation/form";
		}

		reservationRepo.save(reservation);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		reservationRepo.deleteById(id);

		return "redirect:/reservation/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}

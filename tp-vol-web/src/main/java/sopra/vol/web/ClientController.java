package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Civilite;
import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.MoyenPaiement;
import sopra.vol.dao.IClientRepository;

@Controller
@RequestMapping("/clientpart")
public class ClientController {
	@Autowired
	private IClientRepository clientRepo;

	public ClientController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesClients", clientRepo.findAll());

		return "clientpart/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monClient", new ClientParticulier());
		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("moyenPaiements", MoyenPaiement.values());

		return "clientpart/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Client> optClient = clientRepo.findById(id);

		if (optClient.isPresent()) {
			model.addAttribute("monClient", optClient.get());
		}

		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("moyenPaiements", MoyenPaiement.values());

		return "clientpart/form";
	}

//	@PostMapping("/save")
//	public String save(@RequestParam Long id, @RequestParam int version, @RequestParam Civilite civilite, @RequestParam String nom, @RequestParam String prenom, @RequestParam int age, @RequestParam String from) {
//
//		Eleve eleve = null;
//
//		if (from.contentEquals("add")) {
//			eleve = new Eleve(civilite, nom, prenom, age);
//		} else {
//			eleve = new Eleve(id, version, civilite, nom, prenom, age);
//		}
//
//		eleveRepo.save(eleve);
//
//		return "redirect:list";
//	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monClient") @Valid Client client, BindingResult result, Model model) {
		new ClientValidator().validate(client, result);

		if (result.hasErrors()) {
			model.addAttribute("civilites", Civilite.values());
			model.addAttribute("moyenPaiements", MoyenPaiement.values());

			return "clientpart/form";
		}

		clientRepo.save(client);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		clientRepo.deleteById(id);

		return "redirect:/clientpart/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}

package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Vol;

public interface IVolRepository extends JpaRepository<Vol, Long> {
}

package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Reservation;

public interface IReservationRepository extends JpaRepository<Reservation, Long> {
}

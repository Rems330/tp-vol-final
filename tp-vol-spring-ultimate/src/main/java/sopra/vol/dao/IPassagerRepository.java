package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sopra.vol.Passager;

public interface IPassagerRepository extends JpaRepository<Passager, Long> {
	
	@Query("select p from Passager p where p.id = :idPassager")
	Passager findbyId(@Param("idPassager") Long id);
	
	@Query("select p from Passager p where p.nom = :nomPassager")
	Passager findbyNom(@Param("nomPassager") String nom);
	
	@Query("select p from Passager p where p.prenom = :prenomPassager")
	Passager findbyPrenom(@Param("prenomPassager") String prenom);
}

package tp.vol;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sopra.vol.Civilite;
import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.MoyenPaiement;
import sopra.vol.dao.IClientRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/application-context.xml")
public class JUnitSpringTest {

	@Autowired
	private IClientRepository clientRepo;
	
@Test
public void  client() {
	
	ClientParticulier belette = new ClientParticulier();
	belette.setNom("Sabarot");
	belette.setPrenom("Sophie");
	belette.setCivilite(Civilite.MLLE);
	belette.setMail("sph.belete@truc.com");
	belette.setTelephone("0658789452");
	belette.setMoyenPaiement(MoyenPaiement.CB);
	
	belette = (ClientParticulier) clientRepo.save(belette);
	Optional <Client> test = clientRepo.findById(belette.getId());
	
	Assert.assertEquals("Sabarot",  belette.getNom());
	Assert.assertEquals("Sophie", belette.getPrenom());
	
	
	
}
}

package tp.vol;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import sopra.vol.Aeroport;
import sopra.vol.Application;
import sopra.vol.Compagnie;
import sopra.vol.Reservation;
import sopra.vol.Ville;
import sopra.vol.Vol;
import sopra.vol.Voyage;
import sopra.vol.VoyageVol;
import sopra.vol.dao.IAeroportRepository;
import sopra.vol.dao.IClientRepository;
import sopra.vol.dao.ICompagnieRepository;
import sopra.vol.dao.IPassagerRepository;
import sopra.vol.dao.IReservationRepository;
import sopra.vol.dao.IVilleRepository;
import sopra.vol.dao.IVolRepository;
import sopra.vol.dao.IVoyageRepository;
import sopra.vol.dao.IVoyageVolRepository;
import sopra.vol.Adresse;
import sopra.vol.Civilite;
import sopra.vol.ClientParticulier;
import sopra.vol.ClientPro;
import sopra.vol.MoyenPaiement;
import sopra.vol.Passager;
import sopra.vol.TypeEntreprise;
import sopra.vol.TypePieceIdentite;

public class TestJpa {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-vol-jpa");
		EntityManager em = null;
		EntityTransaction tx = null;

		IVolRepository volDao = Application.getInstance().getVolDao();
		IAeroportRepository aeroportDao = Application.getInstance().getAeroportDao();
		IClientRepository clientDao = Application.getInstance().getClientDao();
		ICompagnieRepository compagnieDao = Application.getInstance().getCompagnieDao();
		IPassagerRepository passagerDao = Application.getInstance().getPassagerDao();
		IReservationRepository reservationDao = Application.getInstance().getReservationDao();
		IVilleRepository villeDao = Application.getInstance().getVilleDao();
		IVoyageRepository voyageDao = Application.getInstance().getVoyageDao();
		IVoyageVolRepository voyageVolDao = Application.getInstance().getVoyagevolDao();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		try {
			
			Ville ville1 = new Ville();
			ville1.setNom("San Francisco");
			ville1 = (Ville) villeDao.save(ville1);

			Ville ville2 = new Ville();
			ville2.setNom("Paris");
			ville2 = (Ville) villeDao.save(ville2);
			
			Ville ville3 = new Ville();
			ville3.setNom("Hong Kong");
			ville3 = (Ville) villeDao.save(ville3);
			
			ClientParticulier belette = new ClientParticulier();
			belette.setNom("Sabarot");
			belette.setPrenom("Sophie");
			belette.setCivilite(Civilite.MLLE);
			belette.setMail("sph.belete@truc.com");
			belette.setTelephone("0658789452");
			belette.setMoyenPaiement(MoyenPaiement.CB);
			
			Ville ville4 = new Ville();
			ville4.setNom("Sydney");
			ville4 = (Ville) villeDao.save(ville4);
			
			Aeroport aeroport1 = new Aeroport();
			aeroport1.setCode("FRA");
			aeroport1 = (Aeroport) aeroportDao.save(aeroport1);
			

			Adresse adBelette = new Adresse();
			adBelette.setVoie("renardo");
			adBelette.setComplement("bis");
			adBelette.setCodePostal("25589");
			adBelette.setVille("Landes");
			adBelette.setPays("France");
			belette.setPrincipale(adBelette);
			
			belette = (ClientParticulier) clientDao.save(belette);
			
			ClientPro fennec = new ClientPro();
			fennec.setNomEntreprise("Fennec");
			fennec.setNumeroSiret("12563178236");
			fennec.setNumTVA("25135135");
			fennec.setTypeEntreprise(TypeEntreprise.EURL);
			fennec.setMail("ph.fennec@truc.com");
			fennec.setTelephone("0546328978");
			fennec.setMoyenPaiement(MoyenPaiement.VIREMENT);
			
			Adresse adFennec = new Adresse();
			adFennec.setVoie("Bichou");
			adFennec.setComplement("ter");
			adFennec.setCodePostal("12569");
			adFennec.setVille("Floirac");
			adFennec.setPays("France");
			fennec.setFacturation(adFennec);
			
			Adresse proFennec = new Adresse();
			proFennec.setVoie("HelloWorld");
			proFennec.setComplement("a cote de chez moi");
			proFennec.setCodePostal("01236");
			proFennec.setVille("Blaye");
			proFennec.setPays("France");
			fennec.setPrincipale(proFennec);
			

			fennec = (ClientPro) clientDao.save(fennec);
			
			
			Passager belette2 = new Passager();
			belette2.setNom("Sabarot");
			belette2.setPrenom("Sophie");
			belette2.setCivilite(Civilite.MLLE);
			belette2.setMail("sph.belete@truc.com");
			belette2.setTelephone("0658789452");
			belette2.setTypePI(TypePieceIdentite.PASSEPORT);
			belette2.setDateValiditePI(sdf.parse("12/05/2022"));
			belette2.setDtNaissance(sdf.parse("27/09/1993"));
			belette2.setNationalite("Francaise");		
			
			belette2 = passagerDao.save(belette2);
			
			Voyage voyage1 = new Voyage();
			voyage1 = voyageDao.save(voyage1);
			
			Reservation resa1 = new Reservation();
			resa1.setClient(belette);
			resa1.setPassager(belette2);
			resa1.setNumero("12");
			resa1.setStatut(true);
			resa1.setTarif(230F);
			resa1.setTauxTVA(0.20F);
			resa1.setDateReservation(sdf.parse("17/07/2019"));
			resa1.setVoyage(voyage1);
			
			resa1 = reservationDao.save(resa1);

			Aeroport aeroport2 = new Aeroport();
			aeroport2.setCode("CDG");
			aeroport2 = aeroportDao.save(aeroport2);
			
			Aeroport aeroport3 = new Aeroport();
			aeroport3.setCode("HKO");
			aeroport3 = (Aeroport) aeroportDao.save(aeroport3);
			
			Aeroport aeroport4 = new Aeroport();
			aeroport4.setCode("SYD");
			aeroport4 = (Aeroport) aeroportDao.save(aeroport4);
			
			Aeroport aeroport5 = new Aeroport();
			aeroport5.setCode("HBE");
			aeroport5 = (Aeroport) aeroportDao.save(aeroport5);
			
			List<Aeroport> aeroports = new ArrayList<Aeroport>();
			aeroports.add(aeroport1);
			aeroports.add(aeroport5);
			
			ville1.setAeroports(aeroports);
			
			villeDao.save(ville1);
			
			Compagnie compagnie1 = new Compagnie();
			compagnie1.setNomCompagnie("Emirates");
			compagnie1 = (Compagnie) compagnieDao.save(compagnie1);

			
			Vol vol1 = new Vol();
			vol1.setNumero("125");
			vol1.setDateArrivee(sdf.parse("12/10/2019"));
			vol1.setDateDepart(sdf.parse("11/10/2019"));
			vol1.setOuvert(true);
			vol1.setNbPlaces(85);
			vol1.setCompagnie(compagnie1);
			vol1.setArrivee(aeroport4);
			vol1.setDepart(aeroport2);
			vol1 = (Vol) volDao.save(vol1);

			
			VoyageVol voyagevol1 = new VoyageVol();
			voyagevol1.setOrdre(1);
			voyagevol1.setVol(vol1);
			voyagevol1.setVoyage(voyage1);
			voyagevol1 = (VoyageVol) voyageVolDao.save(voyagevol1);

			
			
			
//			
//			List<VoyageVol> voyagevols = new ArrayList<VoyageVol>();
//			voyagevol1.setVoyage(voyage1);
//			List<Vol> vols = new ArrayList<Vol>();
//			vols.add(vol1);
//			
			
			

			tx.commit();

		} catch (Exception e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
				e.printStackTrace();
			}
		} finally {
			if (em != null) {
				em.close();
			}
		}

		emf.close();


	}

}
